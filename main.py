from turtle import Turtle, Screen
from paddle import Paddle
from ball import Ball
from table import Middle, Table
from scoreboard import Scoreboard
import time

def pong_game():
    INITIAL_PADDLE_POSITIONS = [(350, 0), (-350, 0)]

    screen = Screen()
    screen.clear()
    screen.setup(width=800, height=500)
    screen.bgcolor("black")
    screen.title("Pong")
    screen.tracer(0)
    screen.listen()

    r_paddle = Paddle(INITIAL_PADDLE_POSITIONS[0])
    l_paddle = Paddle(INITIAL_PADDLE_POSITIONS[1])

    ball = Ball()
    table = Table()
    middle = Middle()


    screen.onkey(r_paddle.Up, "Up")
    screen.onkey(r_paddle.Down, "Down")
    screen.onkey(l_paddle.Up, "w")
    screen.onkey(l_paddle.Down, "s")

    scoreboard = Scoreboard()

    game_is_on = True

    while game_is_on:
        screen.update()
        time.sleep(ball.move_speed)
        ball.move()
        if ball.ycor() > 220 or ball.ycor() < -220:
            ball.bounce()

        if (ball.xcor() > 320 and r_paddle.distance(ball) < 50) and ball.turn == "right":
            ball.hit()
            print(r_paddle.distance(ball))

        if (ball.xcor() < -320 and l_paddle.distance(ball) < 50) and ball.turn == "left":
            ball.hit()
            print(l_paddle.distance(ball))

        if ball.xcor() > 400:
            ball.reset_ball()
            scoreboard.l_point()
            time.sleep(1)

        if ball.xcor() < -400:
            ball.reset_ball()
            scoreboard.r_point()
            time.sleep(1)

        if scoreboard.l_score == 3 or scoreboard.r_score == 3:
            game_is_on = False 
            scoreboard.game_over()

    screen.onkey(pong_game, "space")
    screen.exitonclick()

pong_game()
