from turtle import Turtle


class Line(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("square")
        self.up()
        self.color("white")
        self.shapesize(stretch_wid=1, stretch_len=100)
        

class Table(Line):
    def __init__(self):
        super().__init__()
        self.goto(0, 250)
        self.stamp()
        self.goto(0,-250)
        self.stamp()

class Middle(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("square")
        self.up()
        self.color("white")
        self.shapesize(stretch_wid=2, stretch_len=0.2)
        self.stamp()
        self.goto(0,100)
        self.stamp()
        self.goto(0,200)
        self.stamp()
        self.goto(0,-100)
        self.stamp()
        self.goto(0,-200)
        self.stamp()



