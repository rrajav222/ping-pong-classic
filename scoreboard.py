from turtle import Turtle

ALIGNMENT = "center"
FONT = ("Courier", 40, "bold")


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.color('white')
        self.hideturtle()
        self.up()
        self.l_score = 0
        self.r_score = 0
        self.updateScore()

    def updateScore(self):
        self.clear()
        self.goto(-100,160)
        self.write(self.l_score, move=False,
                   align=ALIGNMENT, font=FONT)
        self.goto(100,160)
        self.write(self.r_score, move=False,
                   align=ALIGNMENT, font=FONT)

    def l_point(self):
        self.l_score += 1
        self.updateScore()
    
    def r_point(self):
        self.r_score += 1
        self.updateScore()

    def game_over(self):
        self.goto(0, 0)
        self.write("Press [SPACE] key to play again", move=False,
                   align=ALIGNMENT, font=("Courier", 20, "bold"))



