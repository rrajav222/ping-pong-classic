from turtle import Turtle


class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.color("white")
        self.up()
        self.x_move = 10
        self.y_move = 10
        self.turn = "right"
        self.move_speed = 0.05

    def move(self):
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto(new_x, new_y)

    def bounce(self):
        self.y_move *= -1
    
    def hit(self):
        self.x_move *= -1
        self.move_speed *= 0.9
        if self.turn == "right":
            self.turn = "left"
        elif self.turn == "left":
            self.turn = "right"
    
    def reset_ball(self):
        self.home()
        self.hit()
