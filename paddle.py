from turtle import Turtle


class Paddle(Turtle):
    def __init__(self, position):
        super().__init__()
        self.shape("square")
        self.color("white")
        self.up()
        self.shapesize(stretch_wid=5, stretch_len=1)
        self.goto(position)

    def Up(self):
        current_position = self.ycor()
        if current_position < 180:
            self.goto(self.xcor(), current_position + 40)

    def Down(self):
        current_position = self.ycor()
        if current_position > -180:
            self.goto(self.xcor(), current_position - 40)
        # elif current_position > -180:
        #   self.goto(self.paddle.xcor(), current_position - 40)
